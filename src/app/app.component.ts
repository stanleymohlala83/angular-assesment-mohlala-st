import { Inject, Component, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'angular-assesment-mohlala-st';

  track: any;
  slides: any;

  nextSlide: any;
  rightSlide: any;

  dotsNav: any;
  dots: any;

  slideWidth: any;

  constructor(@Inject(DOCUMENT) private document, private elementRef: ElementRef) {

  }

  ngOnInit() {
    this.track = this.document.querySelector('.carousel__track');
    this.slides = Array.from(this.track.children);

    this.nextSlide = this.document.querySelector('.carousel__button--left');
    this.rightSlide = this.document.querySelector('.carousel__button--right');

    this.dotsNav = this.document.querySelector('.carousel__nav');
    this.dots = Array.from(this.dotsNav.children);

    this.slideWidth = this.slides[0].getBoundingClientRect().width;

    this.slides.forEach((slide, index) => {
      slide.style.left = this.slideWidth * index + 'px';
    });
  }

  ngAfterViewInit() {

  }

  nextClick() {
    const current = this.track.querySelector('.current-slide');
    const next = current.nextElementSibling;

    this.movetoSlide(this.track, current, next);
  }

  prevClick() {
    const current = this.track.querySelector(".current-slide");
    const prev = current.previousElementSibling;

    this.movetoSlide(this.track, current, prev);
  }

  movetoSlide(track, current, target) {
    track.style.tranform = 'translateX(' + target.style.left + ')';
    current.classList.remove('current-slide');
    target.classList.add('current-slide');
  }
}
